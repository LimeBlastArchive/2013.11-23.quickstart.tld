       __     _    ___    __      ____    __     _    _
		/    | |  |   |  | (_    _)  /  __) \   | )  /
	   (     | |  |   |  |   |  |   |  /     |  |/  /            Created by:
		\__  | |  |   |  |   |  |   | |      |     (             Daniel Hollands
		   | | |   \_/   |  _|  |_  |  \__   |  |\  \            http://danielhollands.co.uk/
	   ____| |__\       /__(      )__\    )_/   |_)  \_
	   _       ___        ______  _____     ____        __
		)  ____) (__    __)    /  \    |    \  (__    __)
	   (  (___      |  |      /    \   |     )    |  |           For:
		\___  \     |  |     /  ()  \  |    /     |  |           Lime Blast
		____)  )    |  |    |   __   | | |\ \     |  |           http://limeblast.co.uk/
	   (      (_____|  |____|  (__)  |_| |_\ \____|  |____

The purpose of this project is to provide a starting point for all future websites I build.

It started life as a [Bourbon](http://bourbon.io/)-based solution, but after seeing everything [Compass](http://compass-style.org/) is able to do, I've made the switch (Sorry Bourbon, [Your entrance was good... his was better](http://www.youtube.com/watch?v=ZW0NTzt9uaE).)

*(It should be noted that while I've been using Sass for a few months, I'm new to Compass. Because of this, there could lots of things I've done in very silly ways - if this is the case, please feel free to send me a pull request)*

I should probably warn you that everything in this project is quite specific to my own preferences and server requirements, so I doubt anyone other than I would find use for it in it's current state, but you're welcome to take a look at my code and, should you feel the desire, contribute towards it.

Credit where credit is due
--------------------------
None of this would have been possible without the hard work put into all of the projects which make up the bulk of what you see here. My only real contribution to the aforementioned hard work is to bring it all together into a package that is suitable to my own preferences. So a very big **thank you** to everyone and anyone involved in any part of any of the projects that have made this possible.

*(I plan on keeping this up to date, but only so far as my own requirements, so if, say, H5BP version 7 is released, and this project is still using v4, it just means I've not had any web-based work since v4 was released... But as I say, if you do find a use for this, please feel free to send pull requests.)*

Features
--------
* [Compass](http://compass-style.org/).
* [HTML 5 Boilerplate](http://html5boilerplate.com/) (partially via [compass-h5bp](https://github.com/sporkd/compass-h5bp)).
* Useful defaults for vertical rhythm.
* [Susy Responsive grids](http://susy.oddbird.net/), with mobile-first, six-column into twelve-column layout.
* [PT Sans](http://www.fontsquirrel.com/fonts/PT-Sans) base font, loaded from Google Web Fonts.
* [Google Analytics on Steroids](https://github.com/CardinalPath/gas), loaded from their CDN.
* Support for [FireSass](https://addons.mozilla.org/en-us/firefox/addon/firesass-for-firebug/), and other Sass-debug tools.
* Vertical and horizontal debug grids.
* [Bundler](http://gembundler.com/) dependencies installation
* Guard config for Compass and LiveReload.
* *compile* and *deploy* scripts, with support for [git-ftp](https://github.com/resmo/git-ftp).

How to use
----------
### First things first ###

You need to make sure you've got Ruby installed ([which is beyond the scope of this documentation](https://rvm.io/)) and Bundler ([ditto](http://gembundler.com/)). Once this is covered, install all the relevant gems using the following code:

	gem update --system
	bundle install
    
To take advantage of LiveReload, you need to install the [relevent browser extention](http://feedback.livereload.com/knowledgebase/articles/86242-how-do-i-install-and-use-the-browser-extensions-). If you're using remote development server, you'll also need to [provide an ssh tunnel](http://www.electrictoolbox.com/putty-create-ssh-port-tunnel/) for port `35729` to it.

### Getting the show on the road ###

* Set-up the Apache virtual host.
* Clone/download repo into the correct location on your server.
* When you're ready to start editing sass, run the `guard.sh` script from the root directory, or `bundle exec guard`.

### Ready to go deploy ###

Due to the support for FireSass, and the debug grids, provided in this project, I've set-up a separate routine for when making the site ready to go live.

* Set `$qs-debug` in __base.scss_ to `false`.
* Uncomment the Google Analytics code
* Run the `compile.sh` script (to test it locally)

or

* Run the `deploy.sh` script (to compile the code, commit it to the repo and git-ftp push to the server)

To do
-----

* Move susy grids into its own partial
* Add some default form code for both simple and fancy layouts
* Start using [Pure](http://purecss.io/)
* ???
* [Profit!](http://knowyourmeme.com/memes/profit)