## http://compass-style.org/help/tutorials/configuration-reference/

# compass plugins
require 'susy'
require 'compass-h5bp'

# compass settings
sass_dir = 'public_html/scss'
css_dir = 'public_html/css'
images_dir = 'public_html/img'
javascripts_dir = 'public_html/js'
relative_assets = true

# sass settings
sass_options = {:debug_info => true}